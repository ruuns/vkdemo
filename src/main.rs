
#[macro_use]
extern crate c_str_macro;

mod vulkan_renderer;
use sdl2::event::Event;
use vulkan_renderer as vk;

#[derive(Debug)]
enum RuntimeError {
	Render(vk::RenderError),
	Sdl(String),
}

impl From<vk::RenderError> for RuntimeError {
	fn from(e: vk::RenderError) -> Self { RuntimeError::Render(e) }
}

// adf adsf asf
fn application() -> Result<(), RuntimeError> {
	let sdl = sdl2::init().map_err(RuntimeError::Sdl)?;
	let video = sdl.video().map_err(RuntimeError::Sdl)?;
	let window = video.window("vkdemo\n", 800, 600u32)
		.position_centered()
		.vulkan()
		.build()
		.map_err(|e| RuntimeError::Sdl(e.to_string()))?;

	let _renderer = vk::Renderer::new(&window)?;

	let mut event_pump = sdl.event_pump().map_err(RuntimeError::Sdl)?;
	'mainloop: loop {
		for event in event_pump.poll_iter() {
			match event {
				Event::Quit {..} => break 'mainloop,
				_ => {},
			}
		}
	}	

	Ok(())
}


fn main() {
	match application() {
		Ok(_) 	=> println!("Done"),
		Err(e) 	=> println!("Failure: {:?}", e),
	}
}
