use ash::version::{EntryV1_0, InstanceV1_0, DeviceV1_0};
use ash::vk::Handle;
use ash::vk;
use ash::extensions::khr;
use std::ffi::CStr;
use std::collections::BTreeSet;

#[derive(Debug)]
pub enum RenderError {
	Vk(String)
}

impl From<ash::LoadingError> for RenderError {
	fn from(e: ash::LoadingError) -> Self { RenderError::Vk(e.to_string()) }
}

impl From<Failure> for RenderError {
	fn from(e: Failure) -> Self {
		match e {
			Failure::Fault(msg) => 
				RenderError::Vk(msg),
			Failure::Unsupported(msg, properties) => 
				RenderError::Vk(format!("{}: {}", msg, properties.join(","))),			
		}
	}
}

impl From<ash::vk::Result> for RenderError {
	fn from(e: ash::vk::Result) -> Self { RenderError::Vk(e.to_string()) }
}


pub struct MainDevice {
	physical: vk::PhysicalDevice,
	logical: ash::Device,
}

pub struct Surface {
	khr: khr::Surface,
	vk: vk::SurfaceKHR,
}

pub struct Renderer {
	api: ash::Entry,
	instance: ash::Instance,
	main_device: MainDevice,
	graphics_queue: vk::Queue,
	presentation_queue: vk::Queue,
	surface: Surface,
	swapchain: vk::SwapchainKHR,
}

impl std::ops::Drop for Renderer {
	fn drop(&mut self) {
		unsafe {
			self.instance.destroy_instance(None);
		}
	}
}

const DEVICE_EXTENSIONS: [*const i8; 1] = [b"VK_KHR_swapchain\0".as_ptr() as *const i8];
// const VALIDATION_LAYERS: [*const i8; 1] = [b"VK_LAYER_KHRONOS_validation\0".as_ptr() as *const i8];
const VALIDATION_LAYERS: [*const i8; 0] = [];

impl Renderer {
	pub fn new(window: &sdl2::video::Window) -> Result<Self, RenderError> {
		let api = ash::Entry::new()?;
		let app_name = c_str!("VkDemo");
		let engine_name = c_str!("None");

		let instance_extensions = window.vulkan_instance_extensions().map_err(RenderError::Vk)?;
		let raw_instance_extensions : Vec<*const u8> = instance_extensions.iter().map(|name| name.as_ptr()).collect();

		check_instance_extension(&api, &raw_instance_extensions)?;
		check_validation_layers(&api, &VALIDATION_LAYERS[..])?;

		let app_info = vk::ApplicationInfo {
			application_version: vk::make_version(1, 0, 0),                                        
			engine_version: vk::make_version(1, 0, 0),
			api_version: vk::make_version(1, 0, 0),
			p_application_name: app_name.as_ptr(), 
			p_engine_name: engine_name.as_ptr(),

			..vk::ApplicationInfo::default()
		};

		let create_info = vk::InstanceCreateInfo {
			p_application_info: &app_info,
			enabled_extension_count: raw_instance_extensions.len() as u32,
			pp_enabled_extension_names: raw_instance_extensions.as_ptr() as *const *const i8,
			enabled_layer_count: VALIDATION_LAYERS.len() as u32,
			pp_enabled_layer_names: VALIDATION_LAYERS.as_ptr(),
			..vk::InstanceCreateInfo::default()
		};

		let instance = unsafe { api.create_instance(&create_info, None).unwrap() };
		let surface = Self::get_surface(&api, &instance, &window)?;
		let physical_device = Self::get_physical_device(&instance, &surface)?;
		let queue_family_indices = get_queue_families(&instance, &physical_device, &surface).unwrap();
		let logical_device = Self::get_logical_device(&instance, &physical_device, &queue_family_indices)?;
		let graphics_queue = Self::get_graphics_queue(&logical_device, queue_family_indices.graphics_family);
		let presentation_queue = Self::get_graphics_queue(&logical_device, queue_family_indices.presentation_family);
		let swapchain_loader = khr::Swapchain::new(&instance, &logical_device);
		let swapchain = Self::get_swapchain(&swapchain_loader, &window, &surface, &physical_device, &queue_family_indices)?;

		let main_device = MainDevice { physical: physical_device, logical: logical_device };
		Ok(Renderer {api, instance, main_device, graphics_queue, presentation_queue, swapchain, surface})
	}


	fn get_surface(api: &ash::Entry, instance: &ash::Instance, window: &sdl2::video::Window) -> Result<Surface, RenderError> {
		let surface_raw = window.vulkan_create_surface(instance.handle().as_raw() as usize)
			.map_err(RenderError::Vk)?;
		let vk = vk::Handle::from_raw(surface_raw);
		let khr = khr::Surface::new(api, instance);
		Ok(Surface {vk, khr})
	}


	fn get_physical_device(instance: &ash::Instance, surface: &Surface) -> Result<vk::PhysicalDevice, RenderError> {
		let devices = unsafe { 
			instance.enumerate_physical_devices()
				.map_err(|e| RenderError::Vk(format!("PhysicalDeviceError: {}", e)))?
		};
		if devices.is_empty() {
			return Err(RenderError::Vk("No physical device found".to_string()));
		}

		for device in devices.iter() {
			if check_physical_device_suitable(instance, device, surface) {
				return Ok(device.clone())
			}
		}
		Err(RenderError::Vk("No suitable physical device found".to_string()))
	}


	fn get_logical_device(instance: &ash::Instance, physical_device: &vk::PhysicalDevice, queue_family_indices: &QueueFamilyIndices) 
		-> Result<ash::Device, RenderError> 
	{
		let unique_queue_family_indices = queue_family_indices.to_indices_set();
		let mut queue_create_infos = vec![];

		for index in unique_queue_family_indices.iter() {
			let priority = 1.0;
			let queue_create_info = vk::DeviceQueueCreateInfo {
				queue_family_index: index.clone() as u32,
				queue_count: 1,
				p_queue_priorities: &priority,
				..vk::DeviceQueueCreateInfo::default()
			};
			queue_create_infos.push(queue_create_info)
		}

		let create_info = vk::DeviceCreateInfo {
			queue_create_info_count: queue_create_infos.len() as u32,
			p_queue_create_infos: queue_create_infos.as_ptr(),
			enabled_extension_count: DEVICE_EXTENSIONS.len() as u32,
			pp_enabled_extension_names: DEVICE_EXTENSIONS.as_ptr(),
			..vk::DeviceCreateInfo::default()
		};
		
		unsafe {
			instance.create_device(physical_device.clone(), &create_info, None)
				.map_err(|e| RenderError::Vk(e.to_string()))
		}
	}

	fn get_swapchain(swapchain_loader: &khr::Swapchain, window: &sdl2::video::Window, surface: &Surface, physical_device: &vk::PhysicalDevice, indices: &QueueFamilyIndices) 
		-> Result<vk::SwapchainKHR, RenderError> 
	{
		let swap_chain_details = get_swapchain_details(surface, physical_device)?;
		let best_format = choose_best_surface_format(&swap_chain_details.formats[..]);
		let best_presentmode = choose_best_presentation_mode(&swap_chain_details.present_modes[..]);
		let extent = choose_swap_extent(&window, &swap_chain_details.capabilities);

		let mut image_count = swap_chain_details.capabilities.min_image_count + 1;
		if swap_chain_details.capabilities.max_image_count > 0 
				&& swap_chain_details .capabilities.max_image_count < image_count {
			image_count = swap_chain_details .capabilities.max_image_count
		}

		let shared_indices = [indices.graphics_family as u32, indices.presentation_family as u32];		
		let mut create_info = vk::SwapchainCreateInfoKHR::builder()
			.surface(surface.vk)
			.present_mode(best_presentmode)
			.image_format(best_format.format)
			.image_color_space(best_format.color_space)
			.image_extent(extent)
			.image_array_layers(1)
			.image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
			.pre_transform(swap_chain_details.capabilities.current_transform)
			.composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
			.clipped(true)
			.min_image_count(image_count);

		if indices.graphics_family != indices.presentation_family {
			create_info = create_info
				.image_sharing_mode(vk::SharingMode::CONCURRENT)
				.queue_family_indices(&shared_indices[..]);
		} else {
			create_info = create_info.image_sharing_mode(vk::SharingMode::EXCLUSIVE);
		};
		
		let swapchain_info = create_info.build();
		let swapchain = unsafe { swapchain_loader.create_swapchain(&swapchain_info, None)? };
		Ok(swapchain)
	}


	fn get_graphics_queue(logical_device: &ash::Device, queue_index: usize) -> vk::Queue {
		unsafe {
			logical_device.get_device_queue(queue_index as u32, 0)			
		}
	}
 }


pub struct SwapChainDetails {
	capabilities: vk::SurfaceCapabilitiesKHR,
	formats: Vec<vk::SurfaceFormatKHR>,
	present_modes: Vec<vk::PresentModeKHR>,
}

fn get_swapchain_details(surface: &Surface, physical_device: &vk::PhysicalDevice) -> Result<SwapChainDetails, RenderError> {
	unsafe {
		let capabilities = surface.khr.get_physical_device_surface_capabilities(physical_device.clone(), surface.vk)?;
		let formats = surface.khr.get_physical_device_surface_formats(physical_device.clone(), surface.vk)?;
		let present_modes= surface.khr.get_physical_device_surface_present_modes(physical_device.clone(), surface.vk)?;

		if formats.is_empty() || present_modes.is_empty() {
			return Err(RenderError::Vk("Invalid SwapChainDetails retrieved from Vulkan API".to_string()))
		}

		Ok(SwapChainDetails {capabilities, formats, present_modes})
	}
}


 #[derive(Debug)]
enum Failure {
	Fault(String),
	Unsupported(String, Vec<String>),
}

fn check_instance_extension(api: &ash::Entry, check: &Vec<*const u8>) -> Result<bool, Failure> {
	let supported_extensions = api.enumerate_instance_extension_properties()
		.map_err(|e| Failure::Fault(format!("Enumerating InstanceExtension properties failed: {}", e.to_string())))?;
	let mut unsupported = vec![];

'next: 
	for ext in check.iter() {
		let c_ext = unsafe { CStr::from_ptr(ext.clone() as *const i8) };

		for supported in supported_extensions.iter() {
			let c_sup = unsafe { CStr::from_ptr(supported.extension_name.as_ptr()) };
			if c_ext == c_sup {					
				continue 'next
			}								
		}
		unsupported.push(c_ext.to_str().unwrap_or("?").to_string())
	}
	if unsupported.is_empty() == false {
		Err(Failure::Unsupported("Unsupported InstanceExtensions".to_string(), unsupported))
	} else {
		Ok(true)
	}
}

fn check_validation_layers(api: &ash::Entry, check: &[*const i8]) -> Result<bool, Failure> {
	let available_layers = api.enumerate_instance_layer_properties()
		.map_err(|e| Failure::Fault(format!("Enumerating ValidationLayer properties failed: {}", e.to_string())))?;
	let mut unsupported = vec![];

'next: 
	for ext in check.iter() {
		let c_ext = unsafe { CStr::from_ptr(ext.clone()) };

		for layer in available_layers.iter() {
			let c_sup = unsafe { CStr::from_ptr(layer.layer_name.as_ptr()) };
			if c_ext == c_sup {					
				continue 'next
			}								
		}
		unsupported.push(c_ext.to_str().unwrap_or("?").to_string())
	}
	if unsupported.is_empty() == false {
		Err(Failure::Unsupported("Unsupported validation layers".to_string(), unsupported))
	} else {
		Ok(true)
	}
}

fn check_device_extension(instance: &ash::Instance, physical_device: &vk::PhysicalDevice, check: &[*const i8]) -> Result<bool, Failure> {
	let available_extension = unsafe { instance.enumerate_device_extension_properties(physical_device.clone()) }
		.map_err(|e| Failure::Fault(format!("Enumerating DeviceExtension properties failed: {}", e.to_string())))?;
	let mut unsupported = vec![];

	'next: 
	for ext in check.iter() {
		let c_ext = unsafe { CStr::from_ptr(ext.clone()) };

		for layer in available_extension.iter() {
			let c_sup = unsafe { CStr::from_ptr(layer.extension_name.as_ptr()) };
			if c_ext == c_sup {					
				continue 'next
			}								
		}
		unsupported.push(c_ext.to_str().unwrap_or("?").to_string())
	}
	if unsupported.is_empty() == false {
		Err(Failure::Unsupported("Unsupported validation layers".to_string(), unsupported))
	} else {
		Ok(true)
	}	
}
 
struct QueueFamilyIndices {
	graphics_family: usize,
	presentation_family: usize,
}

impl QueueFamilyIndices {
	fn to_indices_set(&self) -> BTreeSet<usize> {
		let mut set = BTreeSet::new();
		set.insert(self.graphics_family);
		set.insert(self.presentation_family);
		set
	}
}

fn get_queue_families(instance: &ash::Instance, physical_device: &vk::PhysicalDevice, surface: &Surface) -> Option<QueueFamilyIndices> {
	let properties = unsafe { 
		instance.get_physical_device_queue_family_properties(physical_device.clone()) 
	};
	
	let mut graphics_index = None;
	let mut presentation_index = None;

	for (pos, property) in properties.iter().enumerate() {
		if property.queue_count < 1 {
			continue
		}

		if property.queue_flags.contains(vk::QueueFlags::GRAPHICS) {
			graphics_index = Some(pos)
		}

		if let Ok(true) = unsafe { surface.khr.get_physical_device_surface_support(physical_device.clone(), pos as u32, surface.vk.clone()) } {
			presentation_index = Some(pos)
		}		
	}

	match (graphics_index, presentation_index) {
		(Some(g), Some(p)) => Some(QueueFamilyIndices {graphics_family: g, presentation_family: p}),
		_ => None
	}
}

fn check_physical_device_suitable(instance: &ash::Instance, physical_device: &vk::PhysicalDevice, surface: &Surface) -> bool {
	if let None = get_queue_families(instance, physical_device, surface) {
		return false
	}

	if check_device_extension(instance, physical_device, &DEVICE_EXTENSIONS[..]).unwrap_or(false) == false {
		return false
	}

	if let Err(_) = get_swapchain_details(surface, physical_device) {
		return false
	}

	return true
}

fn choose_best_surface_format(formats: &[vk::SurfaceFormatKHR]) -> vk::SurfaceFormatKHR {
	if formats.len() == 1 && formats[0].format == vk::Format::UNDEFINED {
		return vk::SurfaceFormatKHR { format: vk::Format::R8G8B8A8_UNORM, color_space: vk::ColorSpaceKHR::SRGB_NONLINEAR };
	}

	for format in formats.iter() {
		if (format.format == vk::Format::R8G8B8A8_UNORM || format.format == vk::Format::B8G8R8A8_UNORM)
			&& format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
		{
			return format.clone()
		}
	}

	return formats[0].clone();
}

fn choose_best_presentation_mode(presentation_modes: &[vk::PresentModeKHR]) -> vk::PresentModeKHR {
	for p in presentation_modes.iter() {
		if *p == vk::PresentModeKHR::MAILBOX {
			return p.clone()
		}
	}

	return vk::PresentModeKHR::FIFO
}

fn choose_swap_extent(window: &sdl2::video::Window, capabilities: &vk::SurfaceCapabilitiesKHR) -> vk::Extent2D {
	let cur_extent = &capabilities.current_extent;
	if cur_extent.width != u32::MAX {
		return *cur_extent
	}
	use std::cmp::{min, max};
	let (width, height) = window.drawable_size();
	vk::Extent2D {
		width: max(capabilities.min_image_extent.width, min(capabilities.max_image_extent.width, width)),
		height: max(capabilities.min_image_extent.height, min(capabilities.max_image_extent.height, height)),
	}
}
